using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace Test
{
    public class GoReact
    {
        ExtentReports extent = null;
        ExtentTest test = null;

        [OneTimeSetUp]
        public void ExtentStart()
        {
            extent = new ExtentReports();
            var htmlReporter = new ExtentHtmlReporter(@"C:\Users\jessa.jane.s.romo\Source\Repos\ConsoleApp1\Test\index.html");
            extent.AttachReporter(htmlReporter);
        }

        [OneTimeTearDown]
        public void ExtentClose()
        {
            extent.Flush();
        }

        [Test]
        public void Instructor_SignUp()
        {
            test = extent.CreateTest("Instructor Sign-Up").Info("Test Started");
            IWebDriver driver = new ChromeDriver();

            //Navigate to log-in page

            driver.Navigate().GoToUrl("https://dev.goreact.com/dashboard/auth/login");
            driver.Manage().Window.Maximize();
            test.Log(Status.Info, "User is in the site.");

            //Elements Login Page
            IWebElement email_textbox = driver.FindElement(By.Id("email"));
            IWebElement pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement signup_btn = driver.FindElement(By.XPath("//button[contains(text(),'Create Account')]"));
            IWebElement login_btn = driver.FindElement(By.XPath("//button/span[contains(text(),'Log In')]"));

            test.Log(Status.Info, "Log in page is displayed.");

            if (email_textbox.Displayed == false && pw_textbox.Displayed == false && signup_btn.Displayed == false && login_btn.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            signup_btn.Click();
            test.Log(Status.Info, "Sign up button is clicked.");

            //EU Verification Page
            IWebElement yes_btn = driver.FindElement(By.XPath("//button[contains(text(),'Yes')]"));
            IWebElement no_btn = driver.FindElement(By.XPath("//button[contains(text(),'No')]"));

            if (yes_btn.Displayed == false && pw_textbox.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            no_btn.Click();
            test.Log(Status.Info, "No button is clicked.");

            //EU Verification Page
            IWebElement student_btn = driver.FindElement(By.XPath("//button[contains(text(),'STUDENT')]"));
            IWebElement instructor_btn = driver.FindElement(By.XPath("//button[contains(text(),'INSTRUCTOR')]"));

            if (student_btn.Displayed == false && instructor_btn.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            instructor_btn.Click();
            test.Log(Status.Info, "Instructor button is clicked.");

            //Create Instructor Account
            IWebElement firstname_textbox = driver.FindElement(By.Id("first-name"));
            IWebElement lastname_textbox = driver.FindElement(By.Id("last-name"));
            IWebElement num_textbox = driver.FindElement(By.Id("phone-number"));
            IWebElement su_email_textbox = driver.FindElement(By.Id("email"));
            IWebElement su_pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement confirmpw_textbox = driver.FindElement(By.Id("confirm-password"));
            IWebElement checkbox13 = driver.FindElement(By.XPath("//input[@type='checkbox']"));
            IWebElement continue_btn = driver.FindElement(By.XPath("//button/span[contains(text(),'Continue')]"));

            test.Log(Status.Info, "User is in Sign-up form.");
            if (firstname_textbox.Displayed == false && lastname_textbox.Displayed == false && num_textbox.Displayed == false && su_email_textbox.Displayed == false && su_pw_textbox.Displayed == false && su_email_textbox.Displayed == false && confirmpw_textbox.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            firstname_textbox.SendKeys("Test Name");
            lastname_textbox.SendKeys("Test");
            num_textbox.SendKeys("+123456789");
            su_email_textbox.SendKeys("Instructor@test.com");
            su_pw_textbox.SendKeys("Qwerty12.");
            confirmpw_textbox.SendKeys("Qwerty12.");
            Thread.Sleep(5000);
            continue_btn.Click();
            checkbox13.Click();
            continue_btn.Click();

            test.Log(Status.Pass, "All the mandatory fields are populated.");

            IWebElement using_t = driver.FindElement(By.Id("org_type_selector"));
            IWebElement type_t = driver.FindElement(By.Id("course_type_selector"));
            IWebElement format_t = driver.FindElement(By.Id("course_format"));

            string x = "Personal Use";
            string y = "Interviewing";
            string z = "Online";

            using_t.Click();
            IWebElement using_ = driver.FindElement(By.XPath("//*[@id='org_type_selector']/ul/li/a/span[contains(text(),'" + x + "')]"));
            using_.Click();

            type_t.Click();
            IWebElement type_ = driver.FindElement(By.XPath("//*[@id='course_type_selector']/ul/li/a/span[contains(text(),'" + y + "')]"));
            type_.Click();

            format_t.Click();
            IWebElement format_ = driver.FindElement(By.XPath("//*[@id='course_format']/ul/li/a/span[contains(text(),'" + z + "')]"));
            format_.Click();
            Thread.Sleep(5000);

            IWebElement continue_btn2 = driver.FindElement(By.XPath("//button/span[contains(text(),'Continue')]"));
            continue_btn2.Click();

            test.Log(Status.Pass, "Instructor Account is Created.");

            driver.Quit();
        }

        [Test]
        public void Instructor_LogIn()
        {
            test = extent.CreateTest("Instructor Log-in").Info("Test Started");
            IWebDriver driver = new ChromeDriver();

            //Navigate to log-in page


            driver.Navigate().GoToUrl("https://dev.goreact.com/dashboard/auth/login");
            driver.Manage().Window.Maximize();
            test.Log(Status.Info, "User is in the site.");
            test.Log(Status.Info, "Log in page is displayed.");

            //Elements Login Page
            IWebElement email_textbox = driver.FindElement(By.Id("email"));
            IWebElement pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement signup_btn = driver.FindElement(By.XPath("//button[contains(text(),'Create Account')]"));
            IWebElement login_btn = driver.FindElement(By.XPath("//button/span[contains(text(),'Log In')]"));

            if (email_textbox.Displayed == false && pw_textbox.Displayed == false && signup_btn.Displayed == false && login_btn.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            email_textbox.SendKeys("TestName@test.com");
            pw_textbox.SendKeys("Qwerty12.");

            login_btn.Click();
            Thread.Sleep(5000);
            test.Log(Status.Pass, "Instructor is successfully logged-in.");
            driver.Quit();
        }

        [Test]
        public void Student_SignUp()
        {
            test = extent.CreateTest("Student Sign-Up").Info("Test Started");
            IWebDriver driver = new ChromeDriver();

            //Navigate to log-in page
            driver.Navigate().GoToUrl("https://dev.goreact.com/join/d3578e35-9c81-4921-83b9-d6771885174b");
            driver.Manage().Window.Maximize();
            test.Log(Status.Info, "User is in the site.");

            //Elements Login Page
            IWebElement email_textbox = driver.FindElement(By.Id("email"));
            IWebElement pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement signup_btn = driver.FindElement(By.XPath("//button[contains(text(),'Create Account')]"));
            IWebElement login_btn = driver.FindElement(By.XPath("//button/span[contains(text(),'Log In')]"));

            test.Log(Status.Info, "Log in page is displayed.");

            if (email_textbox.Displayed == false && pw_textbox.Displayed == false && signup_btn.Displayed == false && login_btn.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            signup_btn.Click();

            Thread.Sleep(10000);
            test.Log(Status.Info, "Sign up button is clicked.");

            IWebElement continue_btn = driver.FindElement(By.XPath("//button[contains(text(),'Continue')]"));
            continue_btn.Click();

            //Create Student Account
            IWebElement firstname_textbox = driver.FindElement(By.Id("first-name"));
            IWebElement lastname_textbox = driver.FindElement(By.Id("last-name"));
            IWebElement su_email_textbox = driver.FindElement(By.Id("email"));
            IWebElement su_pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement confirmpw_textbox = driver.FindElement(By.Id("confirm-password"));
            IWebElement checkbox13 = driver.FindElement(By.XPath("//input[@type='checkbox']"));
            IWebElement continue_btn1 = driver.FindElement(By.XPath("//button/span[contains(text(),'Continue')]"));

            test.Log(Status.Info, "User is in Sign-up form.");
            if (firstname_textbox.Displayed == false && lastname_textbox.Displayed == false && su_email_textbox.Displayed == false && su_pw_textbox.Displayed == false && su_email_textbox.Displayed == false && confirmpw_textbox.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            Thread.Sleep(5000);

            firstname_textbox.SendKeys("Test Name");
            lastname_textbox.SendKeys("Test");
            su_email_textbox.SendKeys("Student@test.com");
            su_pw_textbox.SendKeys("Qwerty12.");
            confirmpw_textbox.SendKeys("Qwerty12.");
            Thread.Sleep(5000);
            continue_btn1.Click();
            Thread.Sleep(5000);
            checkbox13.Click();
            Thread.Sleep(5000);
            continue_btn1.Click();

            test.Log(Status.Pass, "Instructor Account is Created.");
            driver.Quit();
        }

        [Test]
        public void Student_LogIn()
        {
            test = extent.CreateTest("Student Log-in").Info("Test Started");
            IWebDriver driver = new ChromeDriver();

            //Navigate to log-in page
            driver.Navigate().GoToUrl("https://dev.goreact.com/dashboard/auth/login");
            driver.Manage().Window.Maximize();
            test.Log(Status.Info, "User is in the site.");

            //Elements Login Page
            IWebElement email_textbox = driver.FindElement(By.Id("email"));
            IWebElement pw_textbox = driver.FindElement(By.Id("password"));
            IWebElement signup_btn = driver.FindElement(By.XPath("//button[contains(text(),'Create Account')]"));
            IWebElement login_btn = driver.FindElement(By.XPath("//button/span[contains(text(),'Log In')]"));

            test.Log(Status.Info, "Log in page is displayed.");
            if (email_textbox.Displayed == false && pw_textbox.Displayed == false && signup_btn.Displayed == false && login_btn.Displayed == false)
            {
                test.Log(Status.Fail, "All fields are NOT displayed.");
            }
            else
            {
                test.Log(Status.Pass, "All fields are displayed successfully.");
            }

            email_textbox.SendKeys("StudentTestName@test.com");
            pw_textbox.SendKeys("Qwerty12.");
            login_btn.Click();
            Thread.Sleep(5000);
            test.Log(Status.Pass, "Student is successfully logged-in.");

            driver.Quit();
        }
    }
}